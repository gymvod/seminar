B
Na vypracování teoretické části máte 30 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

15b

a) Co je to OOP? Vysvětlete, z čeho se skládá třída a co znamená zapouzdření. Jak se liší private, protected a public. (5b)
	objektově orientované programování, u kterého zapouzdříme stav a chování do objektů, které můžeme opakovaně používat. zvyšuje se míra absktrakce a kód je menší
	třída je žablonou objektu. Definice třídy se skládá z klíčového slova class následovaného názvem třídy a párem složených závorek
	zapouzdření - stav uvnitř třídy zůstává privátní, okolní objekty, komunikující s danou třídou využívají pouze metody public.
	private - přístup má jen ten daný objekt; protected - přístup má daný objekt, ale dovoule tyto atributy dělit; public - přístup mají všechny objekty

b) Co je to v OOP Interface a jak se liší od normální třídy? Jak v C# zajistíme dědičnost (třida vs interface)? (5b)
	Interface je obvykle základní třída s pouze abstraktními členy. 
	Nelze vytvořit instanci rozhraní přímo; třída nebo struktura může implementovat více rozhraní.
	Za název třídy napíšeme dvojtečku a název, od kterého dědíme -> public class A {kód} .... public class B : A {kód}
c) Vysvětlete, jak funguje Váš řadící algoritmus. (5b)
	Algoritmus postupně prochází pole a na dané místo hledá minimum ze zbývajících prvků. Během hledání si uchová index minima. Pokud menší prvek nalezne, provede výměnu s aktuálním prvkem.






