﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_0601
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int[] arr;

        private void btnSort_Click(object sender, EventArgs e)
        {
           
           Random rnd = new Random();
           int length = 0;
            try
            {
                length = Convert.ToInt32(tbxUser.Text);
            }
            catch
            {
                MessageBox.Show("Nic na mě nezkoušej, tohle není číslo");
            }

            arr = new int[length];
            for (int i = 0; i < length; i++)
            {
                arr[i] = Convert.ToInt32(rnd.Next(0, 2000));
            }
            tbxPrinted.Text = String.Join("; ", arr);


            SelectSort(arr);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            tbxSorted.Clear();
            tbxUser.Clear();
            tbxPrinted.Clear();
        }

        private void SelectSort (int [] array)
        {

            int max;
            int temp;
            for (int i = 0; i < array.Length; i++)
            {
                max = i;
                for (int j = i; j < array.Length; j++)
                {
                    if (array[max] < array[j])
                    {
                        max = j;
                    }
                }
                temp = array[i];
                array[i] = array[max];
                array[max] = temp;

            }
            tbxSorted.Text = String.Join("; ", arr);
        }
    }
}
