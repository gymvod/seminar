﻿namespace Test1003
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxSum = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxX = new System.Windows.Forms.TextBox();
            this.tbxN = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSum = new System.Windows.Forms.Button();
            this.tbxConvert = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxConvert = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblConRes = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxA = new System.Windows.Forms.TextBox();
            this.tbxB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblElipRes = new System.Windows.Forms.Label();
            this.btnElip = new System.Windows.Forms.Button();
            this.pnlElip = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbxSum
            // 
            this.tbxSum.Location = new System.Drawing.Point(87, 87);
            this.tbxSum.Name = "tbxSum";
            this.tbxSum.Size = new System.Drawing.Size(137, 20);
            this.tbxSum.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "vypočítám sum (k^x)...zadej n a x";
            // 
            // tbxX
            // 
            this.tbxX.Location = new System.Drawing.Point(87, 61);
            this.tbxX.Name = "tbxX";
            this.tbxX.Size = new System.Drawing.Size(23, 20);
            this.tbxX.TabIndex = 2;
            // 
            // tbxN
            // 
            this.tbxN.Location = new System.Drawing.Point(200, 61);
            this.tbxN.Name = "tbxN";
            this.tbxN.Size = new System.Drawing.Size(24, 20);
            this.tbxN.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(87, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "x";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(211, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "n";
            // 
            // btnSum
            // 
            this.btnSum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSum.Location = new System.Drawing.Point(117, 61);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(77, 23);
            this.btnSum.TabIndex = 6;
            this.btnSum.Text = "Vypočti";
            this.btnSum.UseVisualStyleBackColor = false;
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // tbxConvert
            // 
            this.tbxConvert.Location = new System.Drawing.Point(477, 61);
            this.tbxConvert.Name = "tbxConvert";
            this.tbxConvert.Size = new System.Drawing.Size(182, 20);
            this.tbxConvert.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(471, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(317, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "napiš číslo a já ho převedu do binární nebo šstnáckové soustavy";
            // 
            // cbxConvert
            // 
            this.cbxConvert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxConvert.FormattingEnabled = true;
            this.cbxConvert.Items.AddRange(new object[] {
            "Binární",
            "Hexadecimální"});
            this.cbxConvert.Location = new System.Drawing.Point(667, 61);
            this.cbxConvert.Name = "cbxConvert";
            this.cbxConvert.Size = new System.Drawing.Size(121, 21);
            this.cbxConvert.TabIndex = 10;
            this.cbxConvert.SelectedIndexChanged += new System.EventHandler(this.cbxConvert_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(667, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "vyber soustavu";
            // 
            // lblConRes
            // 
            this.lblConRes.AutoSize = true;
            this.lblConRes.Location = new System.Drawing.Point(602, 94);
            this.lblConRes.Name = "lblConRes";
            this.lblConRes.Size = new System.Drawing.Size(0, 13);
            this.lblConRes.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(234, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Vypočtu obsha elipsy... zadej dělku poloos a a b";
            // 
            // tbxA
            // 
            this.tbxA.Location = new System.Drawing.Point(16, 204);
            this.tbxA.Name = "tbxA";
            this.tbxA.Size = new System.Drawing.Size(35, 20);
            this.tbxA.TabIndex = 14;
            // 
            // tbxB
            // 
            this.tbxB.Location = new System.Drawing.Point(155, 204);
            this.tbxB.Name = "tbxB";
            this.tbxB.Size = new System.Drawing.Size(39, 20);
            this.tbxB.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "poloosa a";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(155, 185);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "poloosa b";
            // 
            // lblElipRes
            // 
            this.lblElipRes.AutoSize = true;
            this.lblElipRes.Location = new System.Drawing.Point(87, 248);
            this.lblElipRes.Name = "lblElipRes";
            this.lblElipRes.Size = new System.Drawing.Size(0, 13);
            this.lblElipRes.TabIndex = 18;
            // 
            // btnElip
            // 
            this.btnElip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnElip.Location = new System.Drawing.Point(62, 201);
            this.btnElip.Name = "btnElip";
            this.btnElip.Size = new System.Drawing.Size(75, 23);
            this.btnElip.TabIndex = 19;
            this.btnElip.Text = "Vypočti";
            this.btnElip.UseVisualStyleBackColor = false;
            this.btnElip.Click += new System.EventHandler(this.btnElip_Click);
            // 
            // pnlElip
            // 
            this.pnlElip.Location = new System.Drawing.Point(19, 282);
            this.pnlElip.Name = "pnlElip";
            this.pnlElip.Size = new System.Drawing.Size(200, 100);
            this.pnlElip.TabIndex = 20;
            this.pnlElip.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlElip_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Test1003.Properties.Resources.unnamed;
            this.pictureBox1.Location = new System.Drawing.Point(276, 129);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(512, 320);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlElip);
            this.Controls.Add(this.btnElip);
            this.Controls.Add(this.lblElipRes);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxB);
            this.Controls.Add(this.tbxA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblConRes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbxConvert);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbxConvert);
            this.Controls.Add(this.btnSum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbxN);
            this.Controls.Add(this.tbxX);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxSum);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxSum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxX;
        private System.Windows.Forms.TextBox tbxN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.TextBox tbxConvert;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxConvert;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblConRes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxA;
        private System.Windows.Forms.TextBox tbxB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblElipRes;
        private System.Windows.Forms.Button btnElip;
        private System.Windows.Forms.Panel pnlElip;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

