﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test1003
{
    class Elipse
    {
        int a;
        int b;

       public Elipse(int a, int b)
        {
            this.a = a;
            this.b = b;
        }

        public int obsah()
        {
            double s = Math.PI * a * b;
            int res = Convert.ToInt32(s);
            return res;
        }
    }
}
