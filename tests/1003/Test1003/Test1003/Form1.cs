﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test1003
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            int x = int.Parse(tbxX.Text);
            int n = int.Parse(tbxN.Text);
            string sum = "";

            for (int i = 1; i <= n; i++)
            {
                if (i == n)
                {
                sum += i + "^" + x;
                    break;
                }
                sum += i + "^" + x + " + ";
            }
            tbxSum.Text = sum;
        }

        private void cbxConvert_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strg = tbxConvert.Text;
            int tried;
            try
            {
                tried = Convert.ToInt32(strg);
               
            }
            catch (Exception)
            {
                MessageBox.Show("neni cislo");
            }
            tried = Convert.ToInt32(strg);
            switch (cbxConvert.SelectedIndex)
            {
                case 0:
                    lblConRes.Text = Convert.ToString(tried, 2);
                    break;
                case 1:
                    string hex = tried.ToString("X");
                    lblConRes.Text = hex;
                    break;
            }
        }

        private void btnElip_Click(object sender, EventArgs e)
        {
            
            int a = int.Parse(tbxA.Text);
            int b = int.Parse(tbxB.Text);
            Elipse elip = new Elipse(a, b);
            lblElipRes.Text = elip.obsah().ToString();
            pnlElip.Invalidate();
        }

        private void pnlElip_Paint(object sender, PaintEventArgs e)
        {
            Pen pen1 = new Pen(Color.DarkBlue, 2);
            Rectangle rect = new Rectangle(10, 10, pnlElip.Width - 12, pnlElip.Height - 12);
            
             e.Graphics.DrawEllipse(pen1, rect);            
        }
    }
}
