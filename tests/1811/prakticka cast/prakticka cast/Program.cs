﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prakticka_cast
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadejte delku pole");
            int c;
            c = int.Parse(Console.ReadLine());
            int[] pole = new int[c];
            Random rnd = new Random();
            for (int i = 0; i < c; i++)
            {
                pole[i] = rnd.Next(0, 30);
            }
           
            printArray(pole);

            List<int> suda = new List<int>();
            List<int> licha = new List<int>();

            foreach (int item in pole)
            {
                if (isLiche(item))
                {
                    licha.Add(item);            
                }
                else
                {
                    suda.Add(item);
                }
            }

            Console.WriteLine("Pocet sudych ciesel je " + suda.Count);
            Console.WriteLine("Pocet lichych ciesel je " + licha.Count);


            printArray(reverse(pole));

            Console.ReadKey();
        }

        static void printArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }


        static bool isLiche(int x)
        {

            if (x % 2 == 0)
            {
                return false;
            }
            return true;
        }


        static int[] reverse(int[] array)
        {

            int[] reversed = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                reversed[array.Length - i - 1] = array[i];

            }

            return reversed;

        }

    }
}
