﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace optn_menu
{
    public partial class Form3 : Form
    {

        Form1 form1;

        public Form3(Form1 form1)
        {
            InitializeComponent();
            this.form1 = form1;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            //button1.BackColor = Color.Lime;
            //button1.ForeColor = Color.Black;


          //  if (!panel2.Controls.Contains(UserControl1.Instance))
            //{
                panel2.Controls.Add(UserControl1.Instance);
                UserControl1.Instance.Dock = DockStyle.Fill;
                UserControl1.Instance.BringToFront();
            //}
            //else
              //  UserControl1.Instance.BringToFront();

          //  if (panel2.Controls.Contains(UserControl1.Instance))
            //{
                button1.BackColor = Color.Lime;
                button1.ForeColor = Color.Black;
                button2.BackColor = Color.Black;
                button2.ForeColor = Color.Lime;
                button3.BackColor = Color.Black;
                button3.ForeColor = Color.Lime;
                button4.BackColor = Color.Black;
                button4.ForeColor = Color.Lime;
                button5.BackColor = Color.Black;
                button5.ForeColor = Color.Lime;
           // }

            //userControl11.Hide();
            //userControl21.Hide();
            //userControl31.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            if (!panel2.Controls.Contains(UserControl1.Instance))
           {
                panel2.Controls.Add(UserControl1.Instance);
                UserControl1.Instance.Dock = DockStyle.Fill;
                UserControl1.Instance.BringToFront();
            }
            else
               UserControl1.Instance.BringToFront();

            if (panel2.Controls.Contains(UserControl1.Instance))
            {
                button1.BackColor = Color.Lime;
                button1.ForeColor = Color.Black;
                button2.BackColor = Color.Black;
                button2.ForeColor = Color.Lime;
                button3.BackColor = Color.Black;
                button3.ForeColor = Color.Lime;
                button4.BackColor = Color.Black;
                button4.ForeColor = Color.Lime;
                button5.BackColor = Color.Black;
                button5.ForeColor = Color.Lime;
            }

            //userControl21.Hide();
            //userControl31.Hide();
            //userControl11.Show();
            //userControl11.BringToFront();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(UserControl2.Instance))
            {
                panel2.Controls.Add(UserControl2.Instance);
                UserControl2.Instance.Dock = DockStyle.Fill;
                UserControl2.Instance.BringToFront();
            }
            else
                UserControl2.Instance.BringToFront();

            if (panel2.Controls.Contains(UserControl2.Instance))
            {
                button2.BackColor = Color.Lime;
                button2.ForeColor = Color.Black;
                button1.BackColor = Color.Black;
                button1.ForeColor = Color.Lime;
                button3.BackColor = Color.Black;
                button3.ForeColor = Color.Lime;
                button4.BackColor = Color.Black;
                button4.ForeColor = Color.Lime;
                button5.BackColor = Color.Black;
                button5.ForeColor = Color.Lime;
            }

            //userControl11.Hide();
            //userControl31.Hide();
            //userControl21.Show();
            //userControl21.BringToFront();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(UserControl3.Instance))
            {
                panel2.Controls.Add(UserControl3.Instance);
                UserControl3.Instance.Dock = DockStyle.Fill;
                UserControl3.Instance.BringToFront();
            }
            else
                UserControl3.Instance.BringToFront();

            if (panel2.Controls.Contains(UserControl3.Instance))
            {
                button3.BackColor = Color.Lime;
                button3.ForeColor = Color.Black;
                button2.BackColor = Color.Black;
                button2.ForeColor = Color.Lime;
                button1.BackColor = Color.Black;
                button1.ForeColor = Color.Lime;
                button4.BackColor = Color.Black;
                button4.ForeColor = Color.Lime;
                button5.BackColor = Color.Black;
                button5.ForeColor = Color.Lime;
            }

            //userControl11.Hide();
            //userControl21.Hide();
            //userControl31.Show();
            //userControl31.BringToFront();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(UserControl4.Instance))
            {
                panel2.Controls.Add(UserControl4.Instance);
                UserControl4.Instance.Dock = DockStyle.Fill;
                UserControl4.Instance.BringToFront();
            }
            else
                UserControl4.Instance.BringToFront();

            if (panel2.Controls.Contains(UserControl4.Instance))
            {
                button4.BackColor = Color.Lime;
                button4.ForeColor = Color.Black;
                button2.BackColor = Color.Black;
                button2.ForeColor = Color.Lime;
                button3.BackColor = Color.Black;
                button3.ForeColor = Color.Lime;
                button1.BackColor = Color.Black;
                button1.ForeColor = Color.Lime;
                button5.BackColor = Color.Black;
                button5.ForeColor = Color.Lime;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(UserControl5.Instance))
            {
                panel2.Controls.Add(UserControl5.Instance);
                UserControl5.Instance.Dock = DockStyle.Fill;
                UserControl5.Instance.BringToFront();
            }
            else
                UserControl5.Instance.BringToFront();

            if (panel2.Controls.Contains(UserControl5.Instance))
            {
                button5.BackColor = Color.Lime;
                button5.ForeColor = Color.Black;
                button2.BackColor = Color.Black;
                button2.ForeColor = Color.Lime;
                button3.BackColor = Color.Black;
                button3.ForeColor = Color.Lime;
                button4.BackColor = Color.Black;
                button4.ForeColor = Color.Lime;
                button1.BackColor = Color.Black;
                button1.ForeColor = Color.Lime;
            }
        }
    }
}
