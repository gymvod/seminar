﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace optn_menu
{
    public partial class Form2 : Form
    {
        Form1 form1;

        //int val;
        public Form2(Form1 form1)
        {
            InitializeComponent();
            this.form1 = form1;

            tabControl1.DrawItem += new DrawItemEventHandler(tabControl1_DrawItem);
        }

        public Form2()
        {
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (form1.button2.Text == "button2")
            {
                if (comboBox21.SelectedIndex == 0)
                {
                    //   button21.Text = "English";
                    form1.ChangeLanguage("English");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 0;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                    //XmlNodeList aNodes = xdoc.SelectNodes("index");
                    //foreach (XmlNode aNode in aNodes)
                    //{


                    //}
                }
                else if (comboBox21.SelectedIndex == 1)
                {
                    //  button21.Text = "Czech";
                    form1.ChangeLanguage("Czech");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 1;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
                else if (comboBox21.SelectedIndex == 2)
                {
                    //  button21.Text = "German";
                    form1.ChangeLanguage("German");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 2;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
            }
            else if (form1.button2.Text == "tlacitko2")
            {
                if (comboBox21.SelectedIndex == 0)
                {
                    //   button21.Text = "English";
                    form1.ChangeLanguage("Czech");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 0;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                    //XmlNodeList aNodes = xdoc.SelectNodes("index");
                    //foreach (XmlNode aNode in aNodes)
                    //{


                    //}
                }
                else if (comboBox21.SelectedIndex == 1)
                {
                    //  button21.Text = "Czech";
                    form1.ChangeLanguage("English");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 1;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
                else if (comboBox21.SelectedIndex == 2)
                {
                    //  button21.Text = "German";
                    form1.ChangeLanguage("German");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 2;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
            }
            else if (form1.button2.Text == "knopf2")
            {
                if (comboBox21.SelectedIndex == 0)
                {
                    //   button21.Text = "English";
                    form1.ChangeLanguage("German");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 0;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                    //XmlNodeList aNodes = xdoc.SelectNodes("index");
                    //foreach (XmlNode aNode in aNodes)
                    //{


                    //}
                }
                else if (comboBox21.SelectedIndex == 1)
                {
                    //  button21.Text = "Czech";
                    form1.ChangeLanguage("Czech");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 1;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
                else if (comboBox21.SelectedIndex == 2)
                {
                    //  button21.Text = "German";
                    form1.ChangeLanguage("English");
                    //XmlDocument xdoc = new XmlDocument();
                    //xdoc.Load("Language.xml");
                    //int ind = 2;
                    //if (xdoc.SelectSingleNode("language/index") == null)
                    //{ }
                    //else
                    //{
                    //    xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
                    //    xdoc.Save("Language.xml");
                    //}
                }
            }
            this.Close();
            
           // System.Windows.Forms.Application.Exit();
        }

        private void comboBox21_SelectedIndexChanged(object sender, EventArgs e)
        {


            //if (comboBox21.SelectedIndex == 0)
            //{
            //    //   button21.Text = "English";
            //    form1.ChangeLanguage("English");
            //    XmlDocument xdoc = new XmlDocument();
            //    xdoc.Load("Language.xml");
            //    int ind = 0;
            //    if (xdoc.SelectSingleNode("language/index") == null)
            //    { }
            //    else
            //    {
            //        xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
            //        xdoc.Save("Language.xml");
            //    }
            //    //XmlNodeList aNodes = xdoc.SelectNodes("index");
            //    //foreach (XmlNode aNode in aNodes)
            //    //{


            //    //}
            //}
            //else if (comboBox21.SelectedIndex == 1)
            //{
            //    //  button21.Text = "Czech";
            //    form1.ChangeLanguage("Czech");
            //    XmlDocument xdoc = new XmlDocument();
            //    xdoc.Load("Language.xml");
            //    int ind = 1;
            //    if (xdoc.SelectSingleNode("language/index") == null)
            //    { }
            //    else
            //    {
            //        xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
            //        xdoc.Save("Language.xml");
            //    }
            //}
            //else if (comboBox21.SelectedIndex == 2)
            //{
            //    //  button21.Text = "German";
            //    form1.ChangeLanguage("German");
            //    XmlDocument xdoc = new XmlDocument();
            //    xdoc.Load("Language.xml");
            //    int ind = 2;
            //    if (xdoc.SelectSingleNode("language/index") == null)
            //    { }
            //    else
            //    {
            //        xdoc.SelectSingleNode("language/index").InnerText = ind.ToString();
            //        xdoc.Save("Language.xml");
            //    }
            //}

        }


        //if (comboBox21.Text == "English")
        //{
        //    val = 0;
        //}
        //else if (comboBox21.Text == "Čeština")
        //{
        //    val = 1;
        //}
        //else if (comboBox21.Text == "Deutsch")
        //{
        //    val = 2;
        //}



        private void Form2_Load(object sender, EventArgs e)
        {
            if (form1.button2.Text == "button2")
            {
                comboBox21.Items.Add("English");
                string[] items ={
                "Čeština",
                "Deutsch"
            };
                comboBox21.Items.AddRange(items);

            }
            else if (form1.button2.Text == "tlacitko2")
            {
                comboBox21.Items.Add("Čeština");
                string[] items ={
                "English",
                "Deutsch"
            };
                comboBox21.Items.AddRange(items);

            }
            else if (form1.button2.Text == "knopf2")
            {
                comboBox21.Items.Add("Deutch");
                string[] items ={
                "Čeština",
                "English"
            };
                comboBox21.Items.AddRange(items);

            }

            comboBox21.SelectedIndex = 0;
            //XmlTextReader xtr = new XmlTextReader("Language.xml");
            //while (xtr.Read())
            //{
            //    if (xtr.NodeType == XmlNodeType.Element && xtr.Name == "index")
            //    {
            //        int s1 = xtr.ReadElementContentAsInt();
            //        comboBox21.SelectedIndex = s1;
            //    }
            //}

            //XmlDocument doc = new XmlDocument();
            //doc.Load("Language.xml");
            //comboBox21.SelectedIndex = val;




        }

        private void tabControl1_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            TabPage _tabPage = tabControl1.TabPages[e.Index];

            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                _textBrush = new SolidBrush(Color.Red);
                g.FillRectangle(Brushes.Gray, e.Bounds);

            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            Font _tabFont = new Font("Arial", 10.0f, FontStyle.Bold, GraphicsUnit.Pixel);

            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));


            //string tabName = tabControl1.TabPages[e.Index].Text;
            //StringFormat stringFormat = new StringFormat();
            //stringFormat.Alignment = StringAlignment.Center;
            //stringFormat.LineAlignment = StringAlignment.Center;
            ////Find if it is selected, this one will be hightlighted...
            //if (e.Index == tabControl1.SelectedIndex)
            //    e.Graphics.FillRectangle(Brushes.LightBlue, e.Bounds);
            //e.Graphics.DrawString(tabName, this.Font, Brushes.Black, tabControl1.GetTabRect(e.Index), stringFormat);


        }


      
    }
    
}
