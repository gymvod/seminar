﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw08
{
    class Calculations
    {
        private int n1, n2, temp, gcd, lcm, a, b;


        public Calculations(int n1, int n2)
        {
            this.n1 = n1;
            this.n2 = n2;

            a = n1;
            b = n2;
            while (b!=0)
            {
                // vydělíme se zbytkem a s béčkem, kde do b uložíme zbytek a do a uložíme původní b..... pak se cyklus opakuje, dokud dělíme se zbytkem
                temp = b;
                b = a % b;
                a = temp;
            }
            // jakmile dělíme bez zbytku, tak a se stává gcd, takže si ho tam uložíme a použijeme pro lcm
            gcd = a;
            // dvě půovdní čísla spolu vynásobím a výsledek vydělím gcd. Z toho mi vznikne lcm
            lcm = (n1 * n2) / gcd;
        }

        public void printGCD()
        {
            Console.WriteLine("Největší společný dělitel je {0}.", gcd);
        }

        public void printLCM()
        {
            Console.WriteLine("Nejmenší společný násobek je {0}.", lcm);
        }
    }
}
