﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw08
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.WriteLine("Vypočítám největší společný dělitel a nejmenší společný násobek dvou čísel!");
            Console.WriteLine("Zadej první číslo: ");
            a = int.Parse(Console.ReadLine());
            Console.WriteLine("Zadej druhé číslo: ");
            b = int.Parse(Console.ReadLine());

            Calculations gcd = new Calculations(a, b);
            Calculations lcm = new Calculations(a, b);

            gcd.printGCD();
            lcm.printLCM();

            Console.ReadKey();
        }
    }
}
