﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace hw04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Jaky prvek fibonacciho posloupnosti chcete vypocitat rekurzivne?");
            int f = int.Parse(Console.ReadLine());
            Console.WriteLine(fibonacciRe(f));
            //  Console.WriteLine(fibonacci(10));
            Console.WriteLine();
            Console.WriteLine("Jaky prvek fibonacciho posloupnosti chcete vypocitat pomoci cyklu?");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine(fibonacci(a));


            Console.ReadKey();
        }

        static int fibonacciRe(int n)
        {
            //bool success = int.TryParse(Console.ReadLine(), out result);
            //int  n = int.Parse(Console.ReadLine());
            if (n < 2)
                return n;
            else
                return fibonacciRe(n - 1) + fibonacciRe(n - 2);
        }

        static int fibonacci(int m)
        {
            int a = 0;
            int b = 1;
            for (int i = 0; i < m; i++)
            {
                int x = a;
                a = b;
                b = x + b;
            }
            return a;


        }
    }
}
