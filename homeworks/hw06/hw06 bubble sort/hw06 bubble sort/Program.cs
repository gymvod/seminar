﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace hw06_bubble_sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] pole = new int[30];
            Random rnd = new Random();
            for (int i = 0; i < 30; i++)
            {
                pole[i] = rnd.Next(0, 30);
            }


            printArray(pole);
            Console.WriteLine();
            bubbleSort(pole);
            printArray(pole);
            Console.ReadKey();
        }


        static void bubbleSort(int[] array)
        {

            int min, temp;
            for (int i = 0; i < array.Length; i++)
            {
                min = i;
                for (int j = i; j < array.Length; j++)
                {
                    if (array[min]>array[j])
                    {
                        min = j;

                        temp = array[i];
                        array[i] = array[min];
                        array[min] = temp;

                    }
                }
      
            }

        }


        static void printArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

    }
}
