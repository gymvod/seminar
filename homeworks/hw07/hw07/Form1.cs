﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace hw07
{
    public partial class Form1 : Form
    {

     


        public Form1()
        {
            InitializeComponent();
        }

        private void buttonAdittion_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBoxFIrstNum.Text);
            int b = Convert.ToInt32(textBoxSecondNum.Text);

            textBoxAnswer.Text = Convert.ToString(a + b);
           // textBoxAnswer.Text += a + b;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textBoxFIrstNum.Clear();
            textBoxSecondNum.Clear();
            textBoxAnswer.Clear();

        }

        private void buttonSubtract_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBoxFIrstNum.Text);
            int b = Convert.ToInt32(textBoxSecondNum.Text);

            textBoxAnswer.Text = Convert.ToString(a - b);
        }

        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBoxFIrstNum.Text);
            int b = Convert.ToInt32(textBoxSecondNum.Text);

            textBoxAnswer.Text = Convert.ToString(a * b);
        }

        private void buttonDivide_Click(object sender, EventArgs e)
        {
            int a = int.Parse(textBoxFIrstNum.Text);
            int b = Convert.ToInt32(textBoxSecondNum.Text);

            if (b == 0)
            {
                textBoxAnswer.Text = "Nemůžeš dělit nulou :D";
            }
            else
            {
                textBoxAnswer.Text = Convert.ToString(a / b);
            }
        }
    }
}
