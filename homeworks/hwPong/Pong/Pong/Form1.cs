﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pong
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        Graphics g;
        int x;
        int y;
        int moveX = 15;
        int moveY = 15;

        public Form1()
        {
            InitializeComponent();
            g = panel1.CreateGraphics();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Brush b = new SolidBrush(Color.Blue);
            Rectangle r = new Rectangle(x, y, 10, 10);
            g.FillEllipse(b, r);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            x += moveX;
            if (x >= panel1.Width-5 || x<0) 
            {
                moveX = -moveX;
            }
            y += moveY;
            if (y >= panel1.Height-5 || y < 0)
            {
                moveY = -moveY;
            }

            panel1.Refresh();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            y = rnd.Next(1, panel1.Height - 10);
            x = rnd.Next(1, panel1.Width - 10);
        }
    }
}
