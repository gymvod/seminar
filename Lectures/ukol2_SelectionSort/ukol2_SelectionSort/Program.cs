﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ukol2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[15];
            Random rnd = new Random();
            for (int i = 0; i < 15; i++)
            {
                arr[i] = rnd.Next(0, 30);
            }
            Console.WriteLine(String.Join(", ", arr));
            Console.WriteLine();
           /* int[] sortedS = SelectionSort(arr);
            Console.WriteLine("Selection sort \n" + String.Join(", ", sortedS));
            Console.WriteLine();*/
            int[] sortedI = InsertionSort(arr, out int[] array);
            Console.WriteLine(String.Join(", ", array));
            Console.WriteLine("Insertion sort \n" + String.Join(", ", sortedI));
            Console.WriteLine();
            int[] sortedB = BubbleSort(arr);
            Console.WriteLine("Bubble sort \n" + String.Join(", ", sortedB));

            Console.ReadKey();
        }



        private static int[] SelectionSort(int[] arr)
        {

            for (int i = 0; i < arr.Length; i++)
            {
                int lowestIndex = i;
                for (int j = i; j < arr.Length; j++)
                {
                    if (arr[j] < arr[lowestIndex])
                        lowestIndex = j; 
                }

                int tmp = arr[lowestIndex];
                arr[lowestIndex] = arr[i];
                arr[i] = tmp;
            }
            return arr;
        }


        private static int[] InsertionSort(int[] arr, out int[] array)
        {
            array = new int[arr.Length];
            arr.CopyTo(array, 0);
         //   array = arr;
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int j = i + 1;
                int tmp = arr[j];
                while (j > 0 && tmp < arr[j - 1])
                {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = tmp;
            }

            return arr;
        }


        private static int [] BubbleSort(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    int tmp;
                    if (arr[j] > arr[j + 1])
                    {
                        tmp = arr[j + 1];
                        arr[j+1] = arr[j];
                        arr[j] = tmp;
                    }
                }
              
            }


            return arr;
        }

    }
}
