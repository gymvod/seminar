﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fractal
{
    public partial class Form1 : Form
    {
        double angle = 30;
        float scale = 3;
        Color colour = Color.Red;
        int lineWidth = 1;

        public Form1()
        {
            InitializeComponent();

            for (int i = 1; i <= 20; i++)
            {
                cbxLineWidth.Items.Add(i);
            }

            cbxLineWidth.SelectedIndex = 0;
        }

        private void nUDAngle_ValueChanged(object sender, EventArgs e)
        {
            angle = Convert.ToDouble(nUDAngle.Value);
            panel1.Refresh();
        }

        private void nUDScale_ValueChanged(object sender, EventArgs e)
        {
            scale = Convert.ToSingle(nUDScale.Value);
            if (scale < 1.3)
                scale = 1.3F;
            panel1.Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            double angleRad = angle * Math.PI / 180;
            DrawBranch(g, new PointF(this.Width / 2, this.Height - 50), new PointF(0, -200), angleRad, scale);
        }


        void DrawBranch(Graphics g, PointF o, PointF v, double a, float s)
        {
            float tempX, tempY;
            Pen pen = new Pen(colour, lineWidth);
            g.DrawLine(pen, o, new PointF(o.X = o.X + v.X, o.Y = o.Y + v.Y));

            if (Math.Abs(v.X) + Math.Abs(v.Y) > 2)
            {
                tempX = v.X;
                tempY = v.Y;

                v.X = Convert.ToSingle(Math.Cos(a) * tempX - Math.Sin(a) * tempY) / s;
                v.Y = Convert.ToSingle(Math.Sin(a) * tempX + Math.Cos(a) * tempY) / s;
                DrawBranch(g, o, v, a, s);

                v.X = Convert.ToSingle(Math.Cos(-a) * tempX - Math.Sin(-a / 2) * tempY) / s;
                v.Y = Convert.ToSingle(Math.Sin(-a) * tempX + Math.Cos(-a / 2) * tempY) / s;
                DrawBranch(g, o, v, a, s);
            }
        }


        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Blue;
            panel1.Refresh();
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Red;
            panel1.Refresh();
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Green;
            panel1.Refresh();
        }

        private void orangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Orange;
            panel1.Refresh();
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Yellow;
            panel1.Refresh();
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colour = Color.Purple;
            panel1.Refresh();
        }

        private void cbxLineWidth_SelectedIndexChanged(object sender, EventArgs e)
        {
            lineWidth = Convert.ToInt32(cbxLineWidth.SelectedItem);
            panel1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MandelbrodForm mf = new MandelbrodForm();
            mf.ShowDialog();
        }
    }
}
