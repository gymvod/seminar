﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class Form1 : Form
    {
        Graphics g;

        public Form1()
        {
            InitializeComponent();
            g = panel1.CreateGraphics();
        }

        private void btnEllip_Click(object sender, EventArgs e)
        {
            Form ellipForm = new EllipseForm();
            ellipForm.ShowDialog();

        }

        private void btnDrawing_Click(object sender, EventArgs e)
        {
            Form drawForm = new DrawingForm();
            drawForm.ShowDialog();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();
            Shape shape = (Shape)rnd.Next(0,3);


            int width = rnd.Next(0, 100);
            int height= rnd.Next(0, 100);

            int x = rnd.Next(0, panel1.Width - width);
            int y = rnd.Next(0, panel1.Height - height);

            int red = rnd.Next(0, 256);
            int green = rnd.Next(0, 256);
            int blue = rnd.Next(0, 256);

            Color colour = Color.FromArgb(red,green,blue);

            switch (shape)
            {
                case Shape.Ellipse:
                    Brush brush1 = new SolidBrush(colour);
                    g.FillEllipse(brush1, x, y, width, height);
                    break;
                case Shape.Rectangle:
                    Brush brush2 = new SolidBrush(colour);
                    g.FillRectangle(brush2, x, y, width, height);
                    break;
                case Shape.Line:
                    Pen pen1 = new Pen(colour);
                    g.DrawLine(pen1, x, y, width, height);
                    break;
                default:
                    break;
            }

            

        }

        enum Shape
        {
            Ellipse,
            Rectangle,
            Line
        }


    }
}
