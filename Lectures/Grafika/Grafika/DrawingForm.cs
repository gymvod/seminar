﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class DrawingForm : Form
    {
        Color selectedColour;
        List<PointWithColour> points = new List<PointWithColour>();
        Graphics g;
        Boolean cursorMoving = false;
        Pen p;
        int x = -1;
        int y = -1;

        public DrawingForm()
        {
            InitializeComponent();

            /*typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
            | BindingFlags.Instance | BindingFlags.NonPublic, null,
            panel1, new object[] { true });*/
            g = panel1.CreateGraphics();
            p = new Pen(Color.Red, 3);
        }

        private void btnCololur_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
              selectedColour = colorDialog1.Color;
            p.Color = colorDialog1.Color;

        }
        private void DrawingForm_MouseDown(object sender, MouseEventArgs e)
        {
            cursorMoving = true;
            x = e.X;
            y = e.Y;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            cursorMoving = false;
            x = -1;
            y = -1;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {

          /* if( e.Button == MouseButtons.Left)
            {
                // Point p = e.Location;
                *//* PointWithColour pointWC = new PointWithColour();
                 pointWC.colour = selectedColour;
                 pointWC.point= e.Location;

                 points.Add(pointWC);

                 panel1.Refresh();*//*

            }*/

            if (x != -1 && y != -1 && cursorMoving == true)
            {
                g.DrawLine(p, new Point(x, y), e.Location);
                x = e.X;
                y = e.Y;
            }

        }

        /*private void panel1_Paint(object sender, PaintEventArgs e)
        {
           *//* Graphics g = e.Graphics;
            for (int i = 1; i < points.Count; i++)
            {
                Pen p = new Pen(points[i-1].colour);
                g.DrawLine(p, points[i-1].point,points[i].point);
            }*//*
        }*/


        private class PointWithColour
        {
            public Point point { get; set; }
            public Color colour { get; set; }
        }

       
    }
}
