﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class EllipseForm : Form
    {
        float width = 0;
        float height = 0;


        public EllipseForm()
        {
            InitializeComponent();
        }

        private void btnDraw_Click(object sender, EventArgs e)
        {
            width = Convert.ToSingle(tbxWidth.Text);
            height = Convert.ToSingle(tbxHeight.Text);
            panel1.Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (height != 0)
            {
                Pen p = new Pen(Color.Red, 2);
                g.DrawEllipse(p, 50, 50, width, height);
            }

        }
    }
}
