﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prace_s_textem
{
    public partial class Form1 : Form
    {
        private string path;

        public Form1()
        {
            InitializeComponent();
        }

        private void nacistSouborToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();
            if (res != DialogResult.OK) return;

            path =  openFileDialog1.FileName;
            textBox1.Text = path;

            StreamReader reader = new StreamReader(path);
            string line = reader.ReadLine();
            while (line != null)
            {
                richTextBox1.Text += line + Environment.NewLine;
                line = reader.ReadLine();
            }
            reader.Close();

        }

        private void ulozitSouborToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (path == null) return;
            StreamWriter writer = new StreamWriter(path);
            string newContent = richTextBox1.Text;
            writer.Write(newContent);
            writer.Close();
        }
    }
}
