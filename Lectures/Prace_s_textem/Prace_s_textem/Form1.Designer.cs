﻿
namespace Prace_s_textem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nacistSouborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ulozitSouborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nactiuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ulozToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(758, 20);
            this.textBox1.TabIndex = 2;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 68);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(758, 370);
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nacistSouborToolStripMenuItem,
            this.ulozitSouborToolStripMenuItem,
            this.souborToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 27);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nacistSouborToolStripMenuItem
            // 
            this.nacistSouborToolStripMenuItem.Name = "nacistSouborToolStripMenuItem";
            this.nacistSouborToolStripMenuItem.Size = new System.Drawing.Size(107, 23);
            this.nacistSouborToolStripMenuItem.Text = "Nacist soubor";
            this.nacistSouborToolStripMenuItem.Click += new System.EventHandler(this.nacistSouborToolStripMenuItem_Click);
            // 
            // ulozitSouborToolStripMenuItem
            // 
            this.ulozitSouborToolStripMenuItem.Name = "ulozitSouborToolStripMenuItem";
            this.ulozitSouborToolStripMenuItem.Size = new System.Drawing.Size(105, 23);
            this.ulozitSouborToolStripMenuItem.Text = "Ulozit soubor";
            this.ulozitSouborToolStripMenuItem.Click += new System.EventHandler(this.ulozitSouborToolStripMenuItem_Click);
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nactiuToolStripMenuItem,
            this.ulozToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(66, 23);
            this.souborToolStripMenuItem.Text = "soubor";
            // 
            // nactiuToolStripMenuItem
            // 
            this.nactiuToolStripMenuItem.Name = "nactiuToolStripMenuItem";
            this.nactiuToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.nactiuToolStripMenuItem.Text = "nacti";
            // 
            // ulozToolStripMenuItem
            // 
            this.ulozToolStripMenuItem.Name = "ulozToolStripMenuItem";
            this.ulozToolStripMenuItem.Size = new System.Drawing.Size(115, 24);
            this.ulozToolStripMenuItem.Text = "uloz";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nacistSouborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ulozitSouborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nactiuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ulozToolStripMenuItem;
    }
}

