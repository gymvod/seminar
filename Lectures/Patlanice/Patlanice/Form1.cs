﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patlanice
{
    public partial class Form1 : Form
    {
        int x, y = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            //kp.DrawRectangle(Pens.Red, 0, 0, 50, 50);
            kp.FillRectangle(Brushes.Maroon, x, y, 50, 50);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                timer1.Enabled = false;
            else
                timer1.Enabled = true;

            //timer1.Enabled = !timer1.Enabled;
        }

        
        

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.KeyCode)
            {
                case Keys.S:
                    y += 10;
                    break;
                case Keys.W:
                    y -= 10;
                    break;
                case Keys.A:
                    x -= 10;
                    break;
                case Keys.D:
                    x += 10;
                    break;
            }

           // if (e.KeyCode == Keys.S)
              //  y += 10;
           // if (e.KeyCode == Keys.W)
              //  y -= 10;
           // if (e.KeyCode == Keys.A)
              //  x -= 10;
           // if (e.KeyCode == Keys.D)
              //  x += 10;


            // e.Handled = true;
            Refresh();
        }

        private void button2DArray_Click(object sender, EventArgs e)
        {
            int[,] array2d = new int[5, 5];
           // int[][] array2d2 = new int[5][];
            //for (int i = 0; i < 5; i++)
            //{
                //array2d2[i] = new int[i * 10];
            //}


            array2d[0, 0] = 1;
            array2d[4, 1] = 2;
            // prvni cislo je sloupec, druhy radek

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array2d.GetLength(1); i++)
            {
                for (int j = 0; j < array2d.GetLength(0); j++)
                {
                    sb.Append(array2d[j, i]);
                    sb.Append(" ");
                }
                sb.AppendLine();
            }

            richTextBox1.Text = sb.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            x += 10;
            y += 10;
            Refresh();
        }
    }
}
