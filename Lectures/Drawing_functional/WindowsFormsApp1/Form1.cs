﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        Graphics g;
        Boolean cursorMoving = false;
        Pen p;
        int x = -1;
        int y = -1;

        public Form1()
        {
            InitializeComponent();
            g = panel1.CreateGraphics();
            p = new Pen(Color.Red, 3);
            
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            cursorMoving = false;
            x = -1;
            y = -1;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            cursorMoving = true;
            x = e.X;
            y = e.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (x != -1 && y != -1 && cursorMoving == true)
            {
                g.DrawLine(p, new Point(x, y), e.Location);
                x = e.X;
                y = e.Y;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult res = colorDialog1.ShowDialog();
            if (res == DialogResult.OK)
                p.Color = colorDialog1.Color;
        }
    }
}
