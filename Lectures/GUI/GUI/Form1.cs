﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void lblName_Click(object sender, EventArgs e)
        {
            tbxName.Focus();
        }

        private void btnName_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Uzivatel kliknul";


            if (tbxName.Text == "")
            {
                MessageBox.Show("Musis zadat jmeno", "Chyba", MessageBoxButtons.OK);
                return;
            }
            DialogResult res = MessageBox.Show("Ahoj " + tbxName.Text, "", MessageBoxButtons.YesNo);
            MessageBox.Show("uzivatel vybral " + res, " odpoved");
            MessageBox.Show("uzivatel vybral " + (res == DialogResult.Yes ? "Ano" : "ne"), " odpoved");

            toolStripStatusLabel1.Text = "Aplikace bezi";

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var item in checkedListBox1.CheckedItems)
            {
                MessageBox.Show("Checked item: " + item.ToString());
            }

            webBrowser1.Url = new Uri("https://seznam.cz");
        }

        private void chbAll_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
                checkedListBox1.SetItemChecked(i, true);
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            MessageBox.Show("Start " + e.Start.ToShortDateString() + " and end " + e.End.ToShortDateString());
        }

        private void zavritToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void barvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var colour = colorDialog1.ShowDialog();
            MessageBox.Show(colour.ToString());
        }

        private void otevritToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                MessageBox.Show(file);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value > 95)
            {
                progressBar1.Value = 0;
            }
            else
            {
                progressBar1.Value += 5;
            }
        }
    }
}
