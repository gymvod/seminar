﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nej_spol_del
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nejvetsi spolecny delitel.");
            Console.WriteLine("cisla budou v intervalu od 1 do {0}", uint.MaxValue);
            int max = int.MaxValue;
            string mes = $"cisla budou v intervalu od 1 do {max}";
            Console.WriteLine(mes);
            Console.WriteLine("Zadej x");
            uint x;
            if (!uint.TryParse(Console.ReadLine(), out x))
            { 
                Console.WriteLine("neni to cislo");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Zadej y");
            uint y;
            if (!uint.TryParse(Console.ReadLine(), out y))
            {
                Console.WriteLine("neni to cele cislo");
                Console.ReadKey();
                return;
            }

            List<uint> delitele = new List<uint>();
            uint min;
            if (x < y)
                min = x;
            else
                min = y;

            uint newMin = Math.Min(x, y);
            for (uint i = 2; i <= min; i++)
            {
                if (x % i == 0 && y % i == 0)
                {
                    delitele.Add(i);
                }
            }

            if (delitele.Count == 0)
                Console.WriteLine("nemaji zadne spolecne delitele");
            else
            {
                Console.WriteLine("vsechny delietele:");
                Console.WriteLine(String.Join(" ", delitele));
                Console.WriteLine("nejvetsi delitel:");
                Console.WriteLine(delitele[delitele.Count - 1]);
            }

            Console.ReadKey();
        }
    }
}
