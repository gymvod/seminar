﻿
namespace ASCII
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLower = new System.Windows.Forms.Button();
            this.btnUpper = new System.Windows.Forms.Button();
            this.btnNumber = new System.Windows.Forms.Button();
            this.tbxRes = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLower
            // 
            this.btnLower.Location = new System.Drawing.Point(42, 12);
            this.btnLower.Name = "btnLower";
            this.btnLower.Size = new System.Drawing.Size(95, 67);
            this.btnLower.TabIndex = 0;
            this.btnLower.Text = "mala abeceda";
            this.btnLower.UseVisualStyleBackColor = true;
            this.btnLower.Click += new System.EventHandler(this.btnLower_Click);
            // 
            // btnUpper
            // 
            this.btnUpper.Location = new System.Drawing.Point(173, 12);
            this.btnUpper.Name = "btnUpper";
            this.btnUpper.Size = new System.Drawing.Size(95, 66);
            this.btnUpper.TabIndex = 1;
            this.btnUpper.Text = "velka abeceda";
            this.btnUpper.UseVisualStyleBackColor = true;
            this.btnUpper.Click += new System.EventHandler(this.btnUpper_Click);
            // 
            // btnNumber
            // 
            this.btnNumber.Location = new System.Drawing.Point(308, 12);
            this.btnNumber.Name = "btnNumber";
            this.btnNumber.Size = new System.Drawing.Size(89, 66);
            this.btnNumber.TabIndex = 2;
            this.btnNumber.Text = "cislice";
            this.btnNumber.UseVisualStyleBackColor = true;
            this.btnNumber.Click += new System.EventHandler(this.btnNumber_Click);
            // 
            // tbxRes
            // 
            this.tbxRes.Location = new System.Drawing.Point(105, 133);
            this.tbxRes.Name = "tbxRes";
            this.tbxRes.Size = new System.Drawing.Size(220, 20);
            this.tbxRes.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 107);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 62);
            this.button1.TabIndex = 4;
            this.button1.Text = "malá polem";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 181);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbxRes);
            this.Controls.Add(this.btnNumber);
            this.Controls.Add(this.btnUpper);
            this.Controls.Add(this.btnLower);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLower;
        private System.Windows.Forms.Button btnUpper;
        private System.Windows.Forms.Button btnNumber;
        private System.Windows.Forms.TextBox tbxRes;
        private System.Windows.Forms.Button button1;
    }
}

