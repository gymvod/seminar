﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASCII
{
    public partial class Form1 : Form
    {
        private Random rnd = new Random();
        char[] abeceda = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        public Form1()
        {
            InitializeComponent();
        }

        private void btnLower_Click(object sender, EventArgs e)
        {
            int num = rnd.Next(97, 123);
            char letter = Convert.ToChar(num);
            tbxRes.Text += letter + " ";
        }

        private void btnUpper_Click(object sender, EventArgs e)
        {
            int num = rnd.Next(65, 91);
            char letter = (char)num ;
            tbxRes.Text += letter + " ";
        }

        private void btnNumber_Click(object sender, EventArgs e)
        {
            int num = rnd.Next(0, 10);
            tbxRes.Text += num + " ";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num = rnd.Next(0, 26);
            tbxRes.Text += abeceda[num].ToString() + " ";

        }
    }
}
