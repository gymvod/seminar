﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ukol1_poleASeznam
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] C = new int [50];
            Random rnd = new Random();
            List<int> P = new List<int>();
            int SP = 0;
            int SC = 0;

            for (int i = 0; i < C.Length; i++)
            {
                C[i] = rnd.Next(1, 50);
                SC += C[i];
            }

            Console.WriteLine(String.Join(", ", C));

            foreach (int item in C)
            {
                if (isPrime(item) == true)
                { 
                    P.Add(item);
                    SP += item;
                }
            }

            Console.WriteLine();
            P.Sort();
            Console.WriteLine(String.Join(", ", P));
            Console.WriteLine("SC = " + SC + "\nSP = " + SP);



            Console.ReadKey();
        }

        private static bool isPrime (int x)
        {
            if (x <= 1)
                return false;

            if (x == 2)
                return true;

            for (int i = 2; i <= Math.Sqrt(x); i++)
            {
                if (x % i == 0)
                {
                    return false;
                }
                          
            }
            return true;
        }
    }
}
