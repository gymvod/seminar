﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_opakovani
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();

        public Form1()
        {
            InitializeComponent();

            List<IShape> instance = new List<IShape>();

            for (int i = 0; i < 20; i++)
            {
                Shape shape = (Shape)rnd.Next(0, 3);
                switch(shape)
                {
                    case Shape.circle:
                        {
                            decimal radius = rnd.Next(1, 100);
                            IShape circ = new Circle(radius);
                            instance.Add(circ);
                        }
                        break;

                    case Shape.regtangle:
                        {
                            decimal a = rnd.Next(1, 100);
                            decimal b = rnd.Next(1, 100);
                            IShape rect = new Rectangle(a, b);
                            instance.Add(rect);
                        }
                        break;

                    case Shape.square:
                        {
                            decimal a = rnd.Next(1, 100);
                            IShape squar = new Square(a);
                            instance.Add(squar);
                        }
                        break;
                }

            }

            foreach (IShape item in instance)
            {
                richTextBox1.Text += item.GetInfo() + Environment.NewLine;
                richTextBox2.Text += item.CalculateArea() + Environment.NewLine;
            }

        }

       



    }
    enum Shape
    {
        circle,
        regtangle,
        square
    }
}
