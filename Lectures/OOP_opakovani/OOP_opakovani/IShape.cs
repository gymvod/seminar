﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_opakovani
{
   public interface IShape
    {
	    decimal CalculateArea();
        string GetInfo();
    }
}

