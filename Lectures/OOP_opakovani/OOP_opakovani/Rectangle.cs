﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_opakovani
{
    class Rectangle : IShape
    {
        protected decimal a;
        protected decimal b;

        public Rectangle(decimal a, decimal b)
        {
            this.a = a;
            this.b = b;
        }

        public decimal CalculateArea()
        {
            decimal area = a * b;
            return area;
        }

        public virtual string GetInfo()
        {
            string info = "Ja jsem obdelnik :D";
            return info;
        }
    }
}
