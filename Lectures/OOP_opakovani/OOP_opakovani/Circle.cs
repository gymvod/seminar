﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_opakovani
{
    class Circle : IShape
    {
        private decimal r;

        public Circle(decimal r)
        {
            this.r = r;
        }

        public decimal CalculateArea()
        {
            decimal area = r * r * Convert.ToDecimal(Math.PI);
            return Convert.ToDecimal(area);
        }

        public string GetInfo()
        {
            string info = "Ja jsem kruh :D";
            return info;
        }
    }
}
