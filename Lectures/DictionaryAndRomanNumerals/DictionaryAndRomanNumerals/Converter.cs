﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DictionaryAndRomanNumerals
{
    class Converter : IRomanCovnerter, IBinaryConverter
    {

        private readonly Dictionary<char, int> RomanToArab = new Dictionary<char, int>
        {
            {'I', 1},
            {'V', 5},
            {'X', 10},
            {'L', 50},
            {'C', 100},
            {'D', 500},
            {'M', 1000},
        };

        private readonly Dictionary<int, string> ArabToRoman = new Dictionary<int, string>
        {
            { 1000, "M" },
            { 900, "CM" },
            { 500, "D" },
            { 400, "CD" },
            { 100, "C" },
            { 90, "XC" },
            { 50, "L" },
            { 40, "XL" },
            { 10, "X" },
            { 9, "IX" },
            { 5, "V" },
            { 4, "IV" },
            { 1, "I" },
        };



        public int FromRoman(string roman)
        {
            int total = 0;

            for (int i = 0; i < roman.Length; i++)
            {

                if (i > 0 && RomanToArab[roman[i]] > RomanToArab[roman[i - 1]])
                {
                    total += RomanToArab[roman[i]] - 2 * RomanToArab[roman[i - 1]];
                }
                else
                {
                    total += RomanToArab[roman[i]];
                }

            }

            return total;
        }

        public string ToRoman(int number)
        {
            StringBuilder roman = new StringBuilder();

            foreach (KeyValuePair<int, string> item in ArabToRoman)
            {
                while (number >= item.Key)
                {
                    roman.Append(item.Value);
                    number -= item.Key;
                }
            }
            return roman.ToString();
        }

        public int BinaryToDecimal(string binaryNumber)
        {
            char[] binArr = binaryNumber.ToCharArray();
            Array.Reverse(binArr);
            double dec = 0;

            for (int i = 0; i < binArr.Length; i++)
            {
                if (binArr[i] == '1')
                    dec += Math.Pow(2, i);
            }

            return (int)dec;
        }

        public int BinaryToDecimalEasy(string binaryNumber)
        {
            return Convert.ToInt32(binaryNumber, 2);
        }

        public string DecimalToBinary(int number)
        {
            List<int> binaryNumber = new List<int>();

            while (number > 0)
            {
                binaryNumber.Add(number % 2);
                //    binaryNum.Prepend(number % 2);  bude se to davat od konce, takze nemusim reversovat
                number = number / 2;
            }

            binaryNumber.Reverse();
            return String.Join("", binaryNumber);
        }

        public string DecimalToBinaryEasy(int number)
        {
            return Convert.ToString(number, 2);
        }

        public string DecimalToHexadecimal(int number)
        {
            List<int> Hexa = new List<int>();

            while (number > 0)
            {
                Hexa.Add(number % 16);
                number = number / 16;
                Hexa.Prepend(number);
            }

            return String.Join(", ", Hexa);
        }

        public int HexadecimalToDecimal(string hexNumber)
        {
            return Convert.ToInt32(hexNumber, 16);
        }

    }
}
