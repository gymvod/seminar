﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DictionaryAndRomanNumerals
{
    public partial class Form1 : Form
    {
        Converter converter;

        public Form1()
        {
            InitializeComponent();
// -------------- Dictionary basics -------------------------------------
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("mon", "Monday");
            dic.Add("tue", "Tuesday");
            dic.Add("wed", "Wednesday");
            dic.Add("thu", "Thursday");
            dic.Add("fri", "Friday");

           lblDic.Text = dic["mon"];

            // lblDic.Text = dic["sat"];
            if (dic.ContainsKey("sat"))
                lblDic.Text = dic["sat"];

            dic.Remove("fri");
            //lblDic.Text = dic["fri"];

            lblDic.Text = "";
            foreach (KeyValuePair<string, string> item in dic)
            {
                lblDic.Text += $"Key={item.Key}, Value={item.Value}; ";
            }
            // -----------------------------------------------------------------------------

            converter = new Converter();

        }

        private void btnToRoman_Click(object sender, EventArgs e)
        {
            string input = tbxArab.Text;
            int number;
            if (!int.TryParse(input, out number))
                lblResult.Text = "Error";

            lblResult.Text = converter.ToRoman(number);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string input = tbxArab.Text;

            lblResult.Text = converter.FromRoman(input).ToString();
        }

        private void btnBin_Click(object sender, EventArgs e)
        {
            string input = tbxBin.Text;
            int number;
            if (!int.TryParse(input, out number))
            {

            }

            lblBin.Text = converter.DecimalToBinary(number);
        }

        private void btnToHex_Click(object sender, EventArgs e)
        {
            string input = tbxBin.Text;
            int number;
            if (!int.TryParse(input, out number))
            {

            }

            lblBin.Text = converter.DecimalToHexadecimal(number);
        }

        private void btnToDec_Click(object sender, EventArgs e)
        {
            string input = tbxBin.Text;
            lblBin.Text = converter.BinaryToDecimal(input).ToString();
        }

        private void btnHexToDec_Click(object sender, EventArgs e)
        {
            string input = tbxBin.Text;
            lblBin.Text = converter.HexadecimalToDecimal(input).ToString();

        }
    }
}
