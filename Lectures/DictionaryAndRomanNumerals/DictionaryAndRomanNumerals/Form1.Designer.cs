﻿namespace DictionaryAndRomanNumerals
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDic = new System.Windows.Forms.Label();
            this.tbxArab = new System.Windows.Forms.TextBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnToRoman = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tbxBin = new System.Windows.Forms.TextBox();
            this.btnBin = new System.Windows.Forms.Button();
            this.lblBin = new System.Windows.Forms.Label();
            this.btnToHex = new System.Windows.Forms.Button();
            this.btnToDec = new System.Windows.Forms.Button();
            this.btnHexToDec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDic
            // 
            this.lblDic.AutoSize = true;
            this.lblDic.Location = new System.Drawing.Point(81, 50);
            this.lblDic.Name = "lblDic";
            this.lblDic.Size = new System.Drawing.Size(35, 13);
            this.lblDic.TabIndex = 0;
            this.lblDic.Text = "label1";
            // 
            // tbxArab
            // 
            this.tbxArab.Location = new System.Drawing.Point(434, 63);
            this.tbxArab.Name = "tbxArab";
            this.tbxArab.Size = new System.Drawing.Size(100, 20);
            this.tbxArab.TabIndex = 1;
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(434, 193);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 2;
            // 
            // btnToRoman
            // 
            this.btnToRoman.Location = new System.Drawing.Point(434, 127);
            this.btnToRoman.Name = "btnToRoman";
            this.btnToRoman.Size = new System.Drawing.Size(75, 23);
            this.btnToRoman.TabIndex = 3;
            this.btnToRoman.Text = "Převeď";
            this.btnToRoman.UseVisualStyleBackColor = true;
            this.btnToRoman.Click += new System.EventHandler(this.btnToRoman_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(619, 151);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbxBin
            // 
            this.tbxBin.Location = new System.Drawing.Point(158, 272);
            this.tbxBin.Name = "tbxBin";
            this.tbxBin.Size = new System.Drawing.Size(100, 20);
            this.tbxBin.TabIndex = 5;
            // 
            // btnBin
            // 
            this.btnBin.Location = new System.Drawing.Point(118, 314);
            this.btnBin.Name = "btnBin";
            this.btnBin.Size = new System.Drawing.Size(75, 23);
            this.btnBin.TabIndex = 6;
            this.btnBin.Text = "Dec to Bin";
            this.btnBin.UseVisualStyleBackColor = true;
            this.btnBin.Click += new System.EventHandler(this.btnBin_Click);
            // 
            // lblBin
            // 
            this.lblBin.AutoSize = true;
            this.lblBin.Location = new System.Drawing.Point(158, 389);
            this.lblBin.Name = "lblBin";
            this.lblBin.Size = new System.Drawing.Size(35, 13);
            this.lblBin.TabIndex = 7;
            this.lblBin.Text = "label1";
            // 
            // btnToHex
            // 
            this.btnToHex.Location = new System.Drawing.Point(118, 343);
            this.btnToHex.Name = "btnToHex";
            this.btnToHex.Size = new System.Drawing.Size(75, 23);
            this.btnToHex.TabIndex = 8;
            this.btnToHex.Text = "Dec to Hex";
            this.btnToHex.UseVisualStyleBackColor = true;
            this.btnToHex.Click += new System.EventHandler(this.btnToHex_Click);
            // 
            // btnToDec
            // 
            this.btnToDec.Location = new System.Drawing.Point(199, 314);
            this.btnToDec.Name = "btnToDec";
            this.btnToDec.Size = new System.Drawing.Size(75, 23);
            this.btnToDec.TabIndex = 9;
            this.btnToDec.Text = "Bin to Dec";
            this.btnToDec.UseVisualStyleBackColor = true;
            this.btnToDec.Click += new System.EventHandler(this.btnToDec_Click);
            // 
            // btnHexToDec
            // 
            this.btnHexToDec.Location = new System.Drawing.Point(200, 343);
            this.btnHexToDec.Name = "btnHexToDec";
            this.btnHexToDec.Size = new System.Drawing.Size(75, 23);
            this.btnHexToDec.TabIndex = 10;
            this.btnHexToDec.Text = "Hex to Dec";
            this.btnHexToDec.UseVisualStyleBackColor = true;
            this.btnHexToDec.Click += new System.EventHandler(this.btnHexToDec_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnHexToDec);
            this.Controls.Add(this.btnToDec);
            this.Controls.Add(this.btnToHex);
            this.Controls.Add(this.lblBin);
            this.Controls.Add(this.btnBin);
            this.Controls.Add(this.tbxBin);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnToRoman);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.tbxArab);
            this.Controls.Add(this.lblDic);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDic;
        private System.Windows.Forms.TextBox tbxArab;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnToRoman;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbxBin;
        private System.Windows.Forms.Button btnBin;
        private System.Windows.Forms.Label lblBin;
        private System.Windows.Forms.Button btnToHex;
        private System.Windows.Forms.Button btnToDec;
        private System.Windows.Forms.Button btnHexToDec;
    }
}

