﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eratosthenovo_sito
{
    interface IPrimeGenerator
    {
        List<int> PrimesFromIntervalOldWay();

        List<int> PrimesFromIntervalEratosthen();
    }
}
