﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eratosthenovo_sito
{
    class Program
    {
        static void Main(string[] args)
        {
            // interval
            int start = ReadNumber("dolni mez");
            int end = ReadNumber("horni mez");

            PrimeGenerator generator = new PrimeGenerator(start, end);
            Stopwatch watch = new Stopwatch();


            watch.Start();
            List <int> primes1 = generator.PrimesFromIntervalOldWay();
            Console.WriteLine(primes1.Count);
            Console.WriteLine(String.Join(", ", primes1));
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalMilliseconds + "ms");

            watch.Reset();
            watch.Start();
            List<int> primes2 = generator.PrimesFromIntervalEratosthen();
            Console.WriteLine(primes2.Count);
            Console.WriteLine(String.Join(", ", primes2));
            watch.Stop();
            Console.WriteLine(watch.Elapsed.TotalMilliseconds + "ms");

            Console.ReadKey();
        }


        private static int ReadNumber(string name)
        {
            int number;
            Console.WriteLine("Zadejte " + name);
            while(!int.TryParse(Console.ReadLine(), out number))
                Console.WriteLine("Zadejte znovu " + name);

            return number;
        }
    }
}
